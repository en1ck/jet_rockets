FactoryBot.define do
  factory :participation_indication do
    value { Random.rand(100) }
    indication
    game_participation
  end
end
