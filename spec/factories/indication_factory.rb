FactoryBot.define do
  factory :indication do
    sequence(:name) { |n| "indication_name_#{n}" }
    value { Random.rand(100) }
  end
end
