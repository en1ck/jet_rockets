FactoryBot.define do
  factory :game_participation do
    game
    player
  end
end
