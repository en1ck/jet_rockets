FactoryBot.define do
  factory :player do
    sequence(:name) { |n| "player_name_#{n}" }
    team
  end
end
