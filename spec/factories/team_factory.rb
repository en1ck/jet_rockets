FactoryBot.define do
  factory :team do
    sequence(:name) { |n| "team_name_#{n}" }
  end
end
