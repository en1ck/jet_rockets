FactoryBot.define do
  factory :game do
    played_at { Time.now - 1.day }
  end
end
