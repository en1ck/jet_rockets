require 'rails_helper'

RSpec.describe Indication, type: :model do
  it do
    should have_db_column(:name)
      .of_type(:string)
      .with_options(null: false)
  end
  it do
    should have_db_column(:value)
      .of_type(:integer)
      .with_options(null: false)
  end
  it { should have_db_index(:name).unique(true) }
end
