require 'rails_helper'

RSpec.describe Player, type: :model do
  it { should have_db_column(:name).of_type(:string).with_options(null: false) }
  it { should belong_to(:team) }

  context 'static methods' do
    describe '.top' do
      let(:teams) { create_list(:team, 2) }
      let!(:first_team_players) { create_list(:player, 2, team: teams.first) }
      let!(:second_team_players) { create_list(:player, 2, team: teams.second) }
      let(:indication) { create(:indication, value: 50) }
      let(:game) { create(:game) }

      subject { described_class.top(indication, options)}

      shared_context :with_played_games do
        before do
          first_team_players.first(2).each do |player|
            participation = create(
              :game_participation,
              player_id: player.id,
              game_id: game.id
            )
            participation_indication = create(
              :participation_indication,
              indication_id: indication.id,
              game_participation_id: participation.id,
              value: 50 + Random.rand(5)
            )
          end
          second_team_players.first(3).each do |player|
            participation = create(
              :game_participation,
              player_id: player.id,
              game_id: game.id
            )
            participation_indication = create(
              :participation_indication,
              indication_id: indication.id,
              game_participation_id: participation.id,
              value: 20 + Random.rand(5)
            )
          end
        end
      end

      context 'for all teams' do
        let(:options) { { top_size: 5 } }

        context 'when there are no indications data' do
          it { should be_empty }
        end

        context 'with indications data' do
          include_context :with_played_games

          it 'should return expected players' do
            expect(subject).to eq(
              first_team_players.first(2) + second_team_players.first(3)
            )
          end
        end
      end

      context 'with specified team' do
        let(:options) { { top_size: 5, team: teams.first } }

        context 'when there are no indications data' do
          it { should be_empty }
        end

        context 'with indications data' do
          include_context :with_played_games

          it 'should return expected players' do
            expect(subject).to eq(first_team_players.first(2))
          end
        end
      end
    end
  end

end
