require 'rails_helper'

RSpec.describe ParticipationIndication, type: :model do
  it do
    should have_db_column(:value)
      .of_type(:integer)
      .with_options(null: false)
  end
  it { should have_db_index(:value) }

  it { should belong_to(:indication) }
  it { should belong_to(:game_participation) }

  context 'static methods' do
    describe '.complete_indication?' do
      let(:game) { create(:game)}
      let(:player) { create(:player) }
      let(:indication) { create(:indication, value: 50) }
      let(:game_participation) do
        create(:game_participation, player_id: player.id, game_id: game.id)
      end

      subject { described_class.complete?(player, indication) }

      context 'with indication' do
        let(:indication_value) { indication.value }

        before do
          create(
            :participation_indication,
            indication_id: indication.id,
            value: indication_value,
            game_participation_id: game_participation.id
          )
        end

        it { should be_truthy }

        context 'but not not completed' do
          let(:indication_value) { 0 }

          it { should be_falsey }
        end
      end

      context 'with no completed indication' do
        it { should be_falsey }
      end
    end

    describe 'completed_in_last_games?' do
      let(:player) { create(:player) }
      let(:indication) { create(:indication, value: 50) }
      let(:games_count) { 5 }


      subject do
        described_class.completed_in_last_games?(
          player,
          indication,
          games_count
        )
      end

      context 'when there are some games with player' do
        let!(:games) do
          games_count.times.map do |game_num|
            create(:game, played_at: game_num.days.ago)
          end
        end
        let!(:game_participations) do
          games.map do |game|
            create(:game_participation, game_id: game.id, player_id: player.id)
          end
        end

        context 'and there are no indications' do
          it { should be_falsey }
        end

        context 'and some indications completed' do
          context 'in last games' do
            before do
              game_participations.each do |participation|
                create(
                  :participation_indication,
                  game_participation_id: participation.id,
                  indication: indication,
                  value: 50
                )
              end
            end

            it { should be_truthy }
          end

          context 'but not in last games' do
            before do
              another_game = create(:game, played_at: (games_count+1).days.ago)
              another_participation = create(
                :game_participation,
                player_id: player.id,
                game_id: another_game.id
              )
              completed_indication = create(
                :participation_indication,
                game_participation_id: another_participation.id,
                indication_id: indication.id,
                value: 50
              )
              game_participations.each do |participation|
                create(
                  :participation_indication,
                  game_participation_id: participation.id,
                  indication_id: indication.id,
                  value: 49
                )
              end
            end

            it { should be_falsey }
          end
        end

        context 'and no indications completed' do
          before do
            game_participations.each do |participation|
              create(
                :participation_indication,
                game_participation_id: participation.id,
                indication: indication,
                value: 49
              )
            end
          end

          it { should be_falsey }
        end
      end

      context 'when there are no games with player' do
        it { should be_falsey }
      end
    end
  end
end
