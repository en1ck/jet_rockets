require 'rails_helper'

RSpec.describe Game, type: :model do
  it do
    should have_db_column(:played_at)
      .of_type(:datetime)
      .with_options(null: false)
  end
end
