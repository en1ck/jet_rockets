require 'rails_helper'

RSpec.describe Team, type: :model do
  it { should have_db_column(:name).of_type(:string).with_options(null: false) }
end
