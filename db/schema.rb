# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_05_04_113623) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "game_participations", force: :cascade do |t|
    t.bigint "player_id"
    t.bigint "game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_id"], name: "index_game_participations_on_game_id"
    t.index ["player_id"], name: "index_game_participations_on_player_id"
  end

  create_table "games", force: :cascade do |t|
    t.datetime "played_at", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "indications", force: :cascade do |t|
    t.string "name", null: false
    t.integer "value", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_indications_on_name", unique: true
  end

  create_table "participation_indications", force: :cascade do |t|
    t.integer "value", null: false
    t.bigint "game_participation_id"
    t.bigint "indication_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["game_participation_id"], name: "index_participation_indications_on_game_participation_id"
    t.index ["indication_id"], name: "index_participation_indications_on_indication_id"
    t.index ["value"], name: "index_participation_indications_on_value"
  end

  create_table "players", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "team_id"
    t.index ["team_id"], name: "index_players_on_team_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "game_participations", "games"
  add_foreign_key "game_participations", "players"
  add_foreign_key "players", "teams"
end
