class CreateIndications < ActiveRecord::Migration[5.2]
  def up
    transaction do
      create_table :indications do |t|
        t.string :name, null: false
        t.integer :value, null: false

        t.timestamps
      end
      add_index :indications, :name, unique: true
    end
  end

  def down
    drop_table :indications
  end
end
