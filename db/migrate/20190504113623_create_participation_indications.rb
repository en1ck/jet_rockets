class CreateParticipationIndications < ActiveRecord::Migration[5.2]
  def up
    transaction do
      create_table :participation_indications do |t|
        t.integer :value, null: false
        t.references :game_participation
        t.references :indication

        t.timestamps
      end
      add_index :participation_indications, :value
    end
  end

  def down
    drop_table :participation_indications
  end
end
