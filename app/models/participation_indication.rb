class ParticipationIndication < ApplicationRecord
  belongs_to :indication
  belongs_to :game_participation

  scope :by_player_id, -> (player_id) do
    joins(:game_participation)
      .where(game_participations: { player_id: player_id })
  end
  scope :by_indication_id, -> (indication_id) do
    joins(:indication).where(indication_id: indication_id)
  end
  scope :completed_for_value, -> (min_value) do
    where('participation_indications.value >= ?', min_value)
  end

  scope :top_by_indicaion_id, -> (indication_id, top_size) do
    limit(top_size).order(value: :desc).where(indication_id: indication_id)
  end

  def self.completed(player, indication)
    by_player_id(player.id)
      .by_indication_id(indication.id)
      .completed_for_value(indication.value)
  end

  def self.complete?(player, indication)
    completed(player, indication).exists?
  end

  def self.completed_in_last_games?(player, indication, games_count)
    completed(player, indication)
      .where(game_participations: { game_id: Game.last_ones(games_count) })
      .exists?
  end
end
