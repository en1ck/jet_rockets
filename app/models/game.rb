class Game < ApplicationRecord
  scope :last_ones, -> (games_count) do
    limit(games_count).order(played_at: :desc)
  end
end
