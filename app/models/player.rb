class Player < ApplicationRecord
  belongs_to :team
  has_many :game_participations
  has_many :participation_indications, through: :game_participations

  scope :by_team_id, -> (team_id) do
    joins(:team).where(teams: { id: team_id })
  end

  def self.top(indication, top_size:, team: nil)
    top_indications = ParticipationIndication
      .top_by_indicaion_id(indication.id, top_size)

    top_players = joins(:participation_indications)
      .where(participation_indications: { id: top_indications })

    team ? top_players.by_team_id(team.id) : top_players
  end
end
